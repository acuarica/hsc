
# Haskell Supercompiler/Rewrite Rules Analysis

## Introduction

Haskell Rewrite Rules compared to Supercompilation Analysis

## Results

We found **2322** Haskell source files that contain rewrite rules.
Taking only one version of each file, we have **324** distinct Haskell source files.
These Haskell source files are spreaded within **162** Cabal packages.
All files in hackage that contains at least a rewrite/rule are in the file 
`has-rules.out`.
Haskell source files filtered by last version are in `has-rules-by-version.out`.


